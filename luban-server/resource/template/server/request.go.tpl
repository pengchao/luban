package request

import "gin-luban-server/model"

type {{.StructName}}Search struct{
    model.{{.StructName}}
    PageInfo
}