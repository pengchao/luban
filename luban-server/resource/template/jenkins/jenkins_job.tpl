<?xml version='1.1' encoding='UTF-8'?>
<flow-definition plugin="workflow-job@2.41">
  <actions/>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties>
    <jenkins.model.BuildDiscarderProperty>
      <strategy class="hudson.tasks.LogRotator">
        <daysToKeep>90</daysToKeep>
        <numToKeep>15</numToKeep>
        <artifactDaysToKeep>-1</artifactDaysToKeep>
        <artifactNumToKeep>-1</artifactNumToKeep>
      </strategy>
    </jenkins.model.BuildDiscarderProperty>
    <hudson.model.ParametersDefinitionProperty>
      <parameterDefinitions>
        <hudson.model.StringParameterDefinition>
          <name>BRANCH</name>
          <description>分支</description>
          <defaultValue>{{.Para.BRANCH}}</defaultValue>
          <trim>false</trim>
        </hudson.model.StringParameterDefinition>
        <me.leejay.jenkins.dateparameter.DateParameterDefinition plugin="date-parameter@0.0.4">
                  <name>currentVersion</name>
                  <description>时间格式</description>
                  <stringLocalDateValue>
                    <stringLocalDate>LocalDateTime.now()</stringLocalDate>
                    <stringDateFormat>yyyyMMddHHmmss</stringDateFormat>
                  </stringLocalDateValue>
        </me.leejay.jenkins.dateparameter.DateParameterDefinition>
        <hudson.model.BooleanParameterDefinition>
          <name>ifRestart</name>
          <description>服务是否只是重启</description>
          <defaultValue>false</defaultValue>
        </hudson.model.BooleanParameterDefinition>
      </parameterDefinitions>
    </hudson.model.ParametersDefinitionProperty>
  </properties>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.92">
    <script>{{.ScriptContent}}</script>
    <sandbox>true</sandbox>
  </definition>
  <triggers/>
  <disabled>false</disabled>
</flow-definition>