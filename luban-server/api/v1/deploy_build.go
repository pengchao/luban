package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags DeployBuild
// @Summary 查找DeployBuildList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDeployBuildParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/build/getDeployBuildList [get]
func GetDeployBuildList(c *gin.Context) {
	var pageInfo request.SearchDeployBuildParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetDeployBuildList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}
// @Tags DeployBuild
// @Summary 查找DeployBuildList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDeployBuildParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/build/getDeployBuildList [get]

func GetDeployAppBranchList(c *gin.Context)  {
	appName := c.Query("apps_name")
	if appName == "" {
		response.FailValidateMessage("应用参数不能为空",c)
		return
	}

	if err, applyBranch := service.GetDeployAppBranchList(appName); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败"+err.Error(), c)
	} else {
		response.OkWithDetailed(response.AppBranchName{BranchNameList: applyBranch}, "查询成功", c)
	}

}


// 通过APP获取git下面的仓库信息 {分支/列表}
func GetAppGitRepoInfo(c *gin.Context)  {
	appName := c.Query("apps_name")
	if appName == "" {
		response.FailValidateMessage("应用参数不能为空",c)
		return
	}

	if err, appRepoInfo := service.GetAppGitRepoInfo(appName); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败"+err.Error(), c)
	} else {
		response.OkWithDetailed(appRepoInfo, "查询成功", c)
	}

}

// @Tags DeployBuild
// @Summary 查找DeployBuildList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDeployBuildParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/build/getApplyVirtualList [get]

func GetAppVirtualList(c *gin.Context)  {
	var params request.ApplyVirtualParams
	_ = c.ShouldBind(&params)
	if code,msg := utils.Validate(&params );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, virtual := service.GetApplyVirtualList(params); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败"+err.Error(), c)
	} else {
		response.OkWithDetailed(response.CMDBDVirtualResponse{Virtual: virtual}, "查询成功", c)
	}
}



// jenkins 回调通知路由
func DeployNotify(c *gin.Context)  {
	buildNumber := c.Query("build_number")
	buildStatus := c.Query("build_status")
	deployType := c.Query("deploy_type")
	jobName := c.Query("job_name")
	// 写入数据库并返回
	if err := service.DeployNotify(jobName, buildNumber, buildStatus, deployType); err != nil {
			global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
			response.FailWithMessage("创建失败"+ err.Error(), c)
	} else {
			response.OkWithDetailed(gin.H{"status": buildStatus,"buildNumber": buildNumber}, "调用成功", c)
	}
}
