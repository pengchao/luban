package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags SysStation
// @heyibo 创建公司岗位
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysStation true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /station/createStation [post]
func CreateStation(c *gin.Context) {
	var R request.StationForm
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	station :=model.SysStation{StationCode: R.StationCode,StationName: R.StationName,Status: R.Status,CompanyCode: R.CompanyCode,DeptCode: R.DeptCode,Remark: R.Remark}
	if err := service.CreateStation(station); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败"+err.Error(), c)
	} else {
		response.OkWithMessage("创建成功",c)
	}
}

// @Tags SysStation
// @heyibo 更新公司岗位
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysStation true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /station/UpdateStation [post]
func UpdateStation(c *gin.Context) {
	var station model.SysStation
	_ = c.ShouldBind(&station)
	if code,msg := utils.Validate(&station);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.UpdateStation(station); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败"+err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags SysStation
// @Summary 分页获取角色列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "页码, 每页大小"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /authority/getStationList [post]
func GetStationList(c *gin.Context) {
	var pageInfo request.SysStationSearch
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetStationInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败"+err.Error(), c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

// @Tags SysStation
// @Summary 删除SysStation
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysStation true "SysStation模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /sysDictionary/deleteStation [delete]
func DeleteStation (c *gin.Context) {
	var station model.SysStation
	_ = c.ShouldBind(&station)
	if err := service.DeleteStation(&station); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("有用户正在使用禁止删除", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags SysStation
// @Summary getStationById
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysStation true "SysStation模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /sysDictionary/getStationById [Get]
func GetStationById (c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)
	if err,results := service.FindStationById(reqId.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithDetailed(response.SysStationResponse{Station: *results},"获取数据失败!",c)
	} else {
		response.OkWithDetailed(response.SysStationResponse{Station: *results},"获取数据成功!",c)
	}
}



// @Tags SysStation
// @Summary getStationByCode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param CompanyCode and DeptCode body model.SysStation true "SysStation模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /sysDictionary/getStationByCode [Get]
func GetStationByCode (c *gin.Context) {
	var R request.GetCompanyAndDeptCode
	_ = c.ShouldBind(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err,results := service.FindStationByCode(R); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(gin.H{"stationList": results},"获取数据成功!",c)
	}
}