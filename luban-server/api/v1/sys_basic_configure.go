package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags SysCompany
// @heyibo 添加公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysCompany true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /company/createCompany [post]
func CreateBasicConfigure(c *gin.Context) {
	var R request.SysBasicConfigureFrom
	_ = c.ShouldBindJSON(&R)
	if code, msg := utils.Validate(&R); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	config :=model.SysBasicConfigure{BasicUser: R.BasicUser,SshType:R.SshType,BasicPasswd: R.BasicPasswd,SshKey:R.SshKey,Purpose: R.Purpose,ProxyHost:R.ProxyHost,ProxyPort:R.ProxyPort,BaseUrl:R.BaseUrl,ServerIdc: R.ServerIdc,Remark: R.Remark,Status: R.Status}
	if err := service.CreateBasicConfig(config); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}


// @Tags CMDBProject
// @Summary 查找ProjectList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchCMDBProjectParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/project/getProjectList [get]
func GetBasicConfigureList(c *gin.Context) {
	var pageInfo request.SysBasicConfigureSearch
	_ = c.ShouldBind(&pageInfo)
	if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetBasicConfigList(pageInfo.SysBasicConfigure,pageInfo.PageInfo, pageInfo.Desc); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}


// @Tags CmdbServer
// @Summary 修改mdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "修改CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/updateServer  [PUT]
func UpdateBasicConfigure(c *gin.Context) {
	var sysbasic model.SysBasicConfigure
	_ = c.ShouldBindJSON(&sysbasic)
	if err := service.UpdateBasicConfig(sysbasic); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}
//updateBasicConfigure


// @Tags CmdbServer
// @Summary 删除CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "删除CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/deleteServer  [DELETE]

func DeleteBasicConfigure(c *gin.Context){
	var sysbasic model.SysBasicConfigure
	_ = c.ShouldBindJSON(&sysbasic)
	if err := service.DeleteBasicConfig(sysbasic); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}