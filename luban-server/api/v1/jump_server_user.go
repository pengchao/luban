package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags JumpServerUser
// @Summary 添加堡垒机服务器
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.JumpServerUserFrom true "空"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/user/createJumpServerUser [post]
func CreateJumpServerUser(c *gin.Context) {
	var R request.JumpServerUserFrom
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.CreateJumpServerUser(R); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags JumpServerUser
// @Summary 更新堡垒机服务器的别名
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.JumpServerUserFrom true "空"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/user/updateJumpServerUser [post]

func UpdateJumpServerUser(c *gin.Context) {
	var R request.UpdateJumpServerUserFrom
	_ = c.ShouldBindJSON(&R)
	if err := service.UpdateJumpServerUser(R.UserName,R.ID,R.VirtualAlias); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags JumpServerUser
// @msp 删除堡垒机服务器
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerUser true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/server/deleteJumpServerUser [delete]

func DeleteJumpServerUser(c *gin.Context) {
	var Ids request.GetById
	_ = c.ShouldBindJSON(&Ids)
	if code,msg := utils.Validate(&Ids );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteJumpServerUser(Ids.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags JumpServerUser
// @Summary 批量删除JumpServerUser
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除SysOperationRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /jump/server/deleteJumpServerUserByIds [delete]
func DeleteJumpServerUserByIds(c *gin.Context) {
	var IDS request.IdsReq
	_ = c.ShouldBindJSON(&IDS)
	if err := service.DeleteJumpServerUserByIds(IDS); err != nil {
		global.GVA_LOG.Error("批量删除失败!", zap.Any("err", err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// @Tags JumpServerUser
// @Summary 查找JumpServerUserList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchJumpServerUserParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router  /jump/server/getJumpServerUserList [get]
func GetJumpServerUserList(c *gin.Context) {
	var pageInfo request.SearchJumpServerUserParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetJumpServerUserInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}


// @Tags JumpServerUser
// @Summary 查找JumpServerUserList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchJumpServerUserParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router  /jump/server/getJumpServerUserById [get]
func GetJumpServerUserById(c *gin.Context) {
	var Info request.GetById
	_ = c.ShouldBind(&Info)
	if code,msg := utils.Validate(&Info );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list := service.GetJumpServerUserById(Info.Id); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
		}, "获取成功", c)
	}
}