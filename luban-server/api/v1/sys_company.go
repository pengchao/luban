package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags SysCompany
// @heyibo 添加公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysCompany true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /company/createCompany [post]
func CreateCompany(c *gin.Context) {
	var R request.CompanyForm
	_ = c.ShouldBindJSON(&R)
	if code, msg := utils.Validate(&R); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	company := model.SysCompany{CompanyCode: R.CompanyCode, CompanyName: R.CompanyName, Status: R.Status, Remark: R.Remark}
	if err := service.CreateConpany(company); err != nil {
		global.GVA_LOG.Error("添加公司失败!", zap.Any("err", err))
		response.FailWithMessage("添加公司失败"+err.Error(), c)
	} else {
		response.OkWithMessage("添加成功", c)
	}
}

// @Tags SysStation
// @heyibo 更新公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysStation true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /station/UpdateStation [post]
func UpdateCompany(c *gin.Context) {
	var company model.SysCompany
	_ = c.ShouldBind(&company)
	if err := service.UpdateConpany(company); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败"+err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags SysStation
// @Summary 分页获取公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "页码, 每页大小"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /authority/getCompanyList [post]
func GetCompanyList(c *gin.Context) {
	var pageInfo request.SysCompanySearch
	_ = c.ShouldBind(&pageInfo)
	if code, msg := utils.Validate(&pageInfo); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, list, total := service.GetConpanyInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败"+err.Error(), c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

// @Tags SysCompany
// @Summary 删除SysCompany
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysCompany true "SysCompany模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /sysDictionary/deleteCompany [delete]
func DeleteCompany(c *gin.Context) {
	var company model.SysCompany
	_ = c.ShouldBind(&company)
	if err := service.DeleteCompany(&company); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("有用户或部门正在使用，删除失败！", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags SysCompany
// @Summary GetCompanyById
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param Id body model.SysCompany true "SysCompany模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /sysDictionary/GetCompanyById [get]
func GetCompanyById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBind(&reqId)
	if code, msg := utils.Validate(&reqId); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, results := service.FindCompanyById(reqId.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(response.SysCompanyResponse{Company: results}, "获取数据成功!", c)
	}
}

// @Tags SysCompany
// @Summary GetCompanyAll
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param body model.SysCompany true "SysCompany模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /sysDictionary/GetCompanyAll [get]
func GetCompany(c *gin.Context) {
	if err, results := service.FindCompanyAllList(); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(gin.H{"companyList": results}, "获取数据成功!", c)
	}
}
