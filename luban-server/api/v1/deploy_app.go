package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags DeployApp
// @msp 创建项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.DeployApp true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/apply/createDeployApp [post]

func CreateDeployApp(c *gin.Context) {
	var R request.DeployAppFrom
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	apply := model.DeployApp{AppsName: R.AppsName,AppsType:R.AppsType,GitlabUrl: R.GitlabUrl,BranchName: R.BranchName,
		ProjectCode: R.ProjectCode,
	}
	if err := service.CreateDeployApp(apply); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}



// @Tags DeployApp
// @msp 删除项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.DeployApp true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/apply/deleteDeployApp [delete]

func DeleteDeployApp(c *gin.Context) {
	var Ids request.GetById
	_ = c.ShouldBindJSON(&Ids)
	if code,msg := utils.Validate(&Ids );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteDeployApp(Ids.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!有账户正在使用", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags DeployApp
// @msp 更新项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.DeployApp true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/apply/updateDeployApp [put]

func UpdateDeployApp(c *gin.Context) {
	var apply model.DeployApp
	_ = c.ShouldBind(&apply)
	if err := service.UpdateDeployApp(apply); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags DeployApp
// @Summary 查找DeployAppList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDeployAppParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/apply/getDeployAppList [get]
func GetDeployAppList(c *gin.Context) {
	var pageInfo request.SearchDeployAppParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetDeployAppInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

//@Tags DeployApp
//@Summary 获取应用树DeployAppTree
//@Security ApiKeyAuth
//@accept application/json
//@Produce application/json
//@Param data body request.Empty true "空"
//@Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
//@Router /deploy/apply/getDeployAppList [get]
func GetAppProjectAuthList(c *gin.Context) {
	var casbin request.ProjectCasbinInReceive
	_ = c.ShouldBind(&casbin)
	if err := utils.Verify(casbin, utils.AuthorityIdVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetAppProjectAuthList(casbin); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     casbin.Page,
			PageSize: casbin.PageSize,
		}, "获取成功", c)
	}
}

// @Tags DeployAppVirtual
// @msp 更新项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.DeployApp true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/apply/updateDeployAppVirtual [put]

//func UpdateDeployAppVirtual(c *gin.Context) {
//	var R request.DeployAppVirtualFrom
//	_ = c.ShouldBind(&R)
//	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
//		response.FailValidateMessage(msg,c)
//		return
//	}
//	if err := service.UpdateDeployAppVirtual(R); err != nil {
//		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
//		response.FailWithMessage("创建失败" + err.Error(), c)
//	} else {
//		response.OkWithMessage("创建成功", c)
//	}
//}


// @Tags DeployAppVirtual
// @Summary 创建项目应用服务器
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.Empty true "空"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/apply/getDeployAppVirtualList [get]

//func GetDeployAppVirtualList(c *gin.Context) {
//	var pageInfo request.SearchDeployAppVirtualParams
//	_ = c.ShouldBind(&pageInfo)
//	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
//		response.FailValidateMessage(msg,c)
//		return
//	}
//	if err, list, total := service.GetDeployAppVirtualList(pageInfo); err != nil {
//		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
//		response.FailWithMessage("获取失败", c)
//	} else {
//		response.OkWithDetailed(response.PageResult{
//			List:     list,
//			Total:    total,
//			Page:     pageInfo.Page,
//			PageSize: pageInfo.PageSize,
//		}, "获取成功", c)
//	}
//}


