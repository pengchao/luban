package model

import "time"

type SftpLs struct {
	Name  string    `json:"name"`
	Path  string    `json:"path"` // including Name
	Size  string    `json:"size"`
	Time  time.Time `json:"time"`
	Mod   string    `json:"mod"`
	IsDir bool      `json:"is_dir"`
}
