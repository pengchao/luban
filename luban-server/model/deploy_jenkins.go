package model

import "gin-luban-server/global"

type DeployAppJenkins struct {
	global.GVA_MODEL
	AppsName          string   `json:"apps_code" form:"apps_code" gorm:"column:apps_code;comment:应用名称"`
	AppsJobName       string   `json:"apps_job_name" form:"apps_job_name" gorm:"column:apps_job_name;comment:jenkinsJob名称"`
	JenkinsView       string   `json:"jenkins_view" form:"jenkins_view" gorm:"column:jenkins_view;comment:jenkins视图"`
	ServerIdc         string   `json:"server_idc" form:"server_idc" gorm:"column:server_idc;comment:机房地址"`
	JenkinsServer     string   `json:"jenkins_server" form:"jenkins_server" gorm:"column:jenkins_server;comment:jenkins服务器地址"`
	JenkinsJobFiles   string   `json:"jenkins_job_files" form:"jenkins_job_files" gorm:"column:jenkins_job_files;type:text;comment:jenkins_job_file"`
	JenkinsJobStatus  string   `json:"jenkins_job_status" form:"jenkins_job_status" gorm:"column:jenkins_job_status;comment:jenkins_job_status"`
}
