package model

import (
	"gin-luban-server/global"
)

type SysLoginLogs struct {
	global.GVA_MODEL
	Username      string `json:"username" form:"username" gorm:"size:128;"`            //用户名
	NickName      string `json:"nick_name"  gorm:"default:系统用户;comment:用户昵称" `         //用户昵称
	Status        *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"` //状态
	Ipaddr        string `json:"ipaddr" form:"ipaddr" gorm:"size:255;"`                //ip地址
	LoginLocation string `json:"loginLocation" gorm:"size:255;"`                       //归属地
	Browser       string `json:"browser" gorm:"size:255;"`                             //浏览器
	Os            string `json:"os" gorm:"size:255;"`                                  //系统
	Platform      string `json:"platform" gorm:"size:255;"`                            // 固件
	DataScope     string `json:"dataScope" gorm:"-"`                                   //数据
	Remark        string `json:"remark" gorm:"size:255;"`                              //备注
	Msg           string `json:"msg" gorm:"size:255;"`
}
