package model

import "gin-luban-server/global"

type  CMDBDatabase struct {
	global.GVA_MODEL
	ClusterName      string `json:"cluster_name" form:"cluster_name" gorm:"size:255;column:cluster_name;comment:集群名称"`
	DatabaseType     string `json:"database_type" form:"database_type"  gorm:"size:55;column:database_type;comment:中间件类型"`
	VIP              string `json:"vip" form:"vip"  gorm:"size:255;column:vip;comment:VIP地址"`
	Port             string `json:"port" form:"port"  gorm:"size:8;column:port;comment:端口地址"`
	EnvName          string `json:"env_name" form:"env_name"  gorm:"size:50;column:env_name;comment:环境名称"`
	CompanyCode      string `json:"company_code" form:"company_code"  gorm:"size:55;column:company_code;comment:公司code"`
	ProjectCode      string `json:"project_code" form:"project_code" gorm:"column:project_code;type:comment:关联标记;type:varchar(255)"`
	IdcLocation      string `json:"idc_location" form:"idc_location"  gorm:"size:50;column:idc_location;comment:Idc位置"`
	DnsName          string `json:"dns_name" form:"dns_name"  gorm:"size:255;column:dns_name;comment:域名地址"`
	InstanceName     string `json:"instance_name" form:"instance_name"  gorm:"size:255;column:instance_name;comment:实例名称" `
	DeployPath       string `json:"deploy_path" form:"deploy_path"  gorm:"size:255;column:deploy_path;comment:部署路径"`
	IpAddress        string `json:"ip_address" form:"ip_address"  gorm:"size:255;column:ip_address;comment:IP地址"`
	DatabaseSize     string `json:"database_size" form:"database_size" gorm:"size:55;column:database_size;comment:数据大小"`
	DatabaseUser     []CMDBDatabaseUser `json:"databaseUser"  gorm:"foreignKey:ClusterName;references:ClusterName;comment:集群名称"`
	SysCompanyInfo   SysCompany       `json:"syscompanyInfo"  gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司code"`
	Project          CMDBProject `json:"project"  gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
}
