package request

import "gin-luban-server/model"


// 证书search
type NginxDomainCertSearch struct{
	model.NginxDomainCert
	PageInfo
}


type NginxPoolSearch struct{
	model.NginxPool
	PageInfo
}

type NginxPoolNodeSearch struct {
	model.NginxPoolNode
	PageInfo
}