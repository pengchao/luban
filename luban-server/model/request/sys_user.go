package request

import (
	"gin-luban-server/model"
	uuid "github.com/satori/go.uuid"
)

// User register structure
type AddUserForm struct {
	Username    string `json:"username" form:"username"  validate:"required,min=3,max=12" label:"用户名"`
	Password    string `json:"password" form:"password" validate:"required,min=6,max=20" label:"密码"`
	Phone       string `json:"phone" form:"phone" validate:"numeric,len=11" label:"手机号"`
	NickName    string `json:"nick_name" form:"nick_name" validate:"required" label:"用户昵称"`
	Sex         string `json:"sex" form:"sex"  validate:"required"`
	Status      *bool  `json:"status" validate:"required"`
	Email       string `json:"email" form:"email"  validate:"required,email" label:"邮箱" `
	DeptCode    string `json:"dept_code" form:"dept_code"  validate:"required" label:"部门编码"`
	StationCode string `json:"station_code" form:"station_code" validate:"required" label:"岗位编码"`
	Remark      string `json:"remark" form:"remark" gorm:"comment:描述"`
	CompanyCode string `json:"company_code" form:"company_code" validate:"required" label:"公司编码"`
	AuthorityId string `json:"authorityId" form:"authorityId"`
}
// User login structure
type Login struct {
	Username  string `json:"username" validate:"required" label:"用户名"`
	Password  string `json:"password" validate:"required,min=6,max=20" label:"密码"`
	Captcha   string `json:"captcha"`
	CaptchaId string `json:"captchaId"`
}

// Modify password structure
type ChangePasswordStruct struct {
	Username    string `json:"username" validate:"required,min=3,max=12" label:"用户名"`
	Password    string `json:"password" validate:"required,min=6,max=20" label:"密码" `
	NewPassword string `json:"newPassword" validate:"nefield=Password" label:"新密码"`
}

// Modify  user's auth structure
type SetUserAuth struct {
	UUID        uuid.UUID `json:"uuid"`
	AuthorityId string    `json:"authorityId"`
}

type SysUserSearch struct {
	model.SysUser
	PageInfo
}