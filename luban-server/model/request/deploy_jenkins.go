package request

type JenkinsJobBuildParams struct {
	AppsJobName string  `json:"apps_job_name" form:"apps_job_name" validate:"required"`
	BranchName  string  `json:"branch_name" form:"branch_name" validate:"required"`
	TagName    string  `json:"tag_name" form:"tag_name"`
	DeployType string  `json:"deploy_type" form:"deploy_type"`
	IfRestart   bool     `json:"if_restart" form:"if_restart"`   // 是否重启

}


type JenkinsJobRollbackParams struct {
	AppsJobName string  `json:"apps_job_name" form:"apps_job_name" validate:"required"`
	Version  string  `json:"version" form:"version" validate:"required"`
}

type JenkinsBuildJobLogsParams struct {
	AppsJobName string  `json:"apps_job_name" form:"apps_job_name" validate:"required"`
	BuildNumber int64     `json:"build_number" form:"apps_job_name" validate:"required"`
	DeployType string  `json:"deploy_type" form:"deploy_type"`
}
