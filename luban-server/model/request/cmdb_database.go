package request

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
)

type DatabaseForm struct {
	global.GVA_MODEL
	ClusterName      string `json:"cluster_name" form:"cluster_name" gorm:"size:255;column:cluster_name;comment:集群名称" validate:"required"`
	DatabaseType     string `json:"database_type" form:"database_type"  gorm:"size:55;column:database_type;comment:中间件类型" validate:"required"`
	VIP              string `json:"vip" form:"vip"  gorm:"size:255;column:vhost;comment:VIP地址" validate:"required,ipv4"`
	Port             string `json:"port" form:"port"  gorm:"size:8;column:port;comment:端口地址" validate:"required"`
	EnvName          string `json:"env_name" form:"env_name"  gorm:"size:50;column:env_name;comment:环境名称" validate:"required"`
	CompanyCode      string `json:"company_code" form:"company_code"  gorm:"size:55;column:company_code;comment:公司code" validate:"required"`
	ProjectCode      string `json:"project_code" form:"project_code" validate:"required" `
	IdcLocation      string `json:"idc_location" form:"idc_location"  gorm:"size:50;column:idc_location;comment:Idc位置" validate:"required"`
	DnsName          string `json:"dns_name" form:"dns_name"  gorm:"size:255;column:dns_name;comment:域名地址" validate:"omitempty"`
	InstanceName     string `json:"instance_name" form:"instance_name"  gorm:"size:255;column:instance_name;comment:实例名称" validate:"omitempty"`
	DeployPath       string `json:"deploy_path" form:"deploy_path"  gorm:"size:255;column:deploy_path;comment:部署路径" validate:"omitempty"`
	IpAddress        string `json:"ip_address" form:"ip_address"  gorm:"size:255;column:ip_address;comment:IP地址" validate:"omitempty"`
	DatabaseSize     string `json:"database_size" form:"database_size" gorm:"size:55;column:database_size;comment:数据大小" validate:"required"`
}

type SearchDatabaseParams struct {
	model.CMDBDatabase
	PageInfo
}

type ExcelDatabase struct {
	FileName string   `json:"fileName" form:"fileName"`
	model.CMDBDatabaseUser
}