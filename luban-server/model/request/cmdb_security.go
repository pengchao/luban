package request

import "gin-luban-server/model"

type CMDBSecuritySearch struct{
	model.CMDBSecurity
	PageInfo
	Desc     bool   `json:"desc"`
}
