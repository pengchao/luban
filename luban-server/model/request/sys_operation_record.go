package request

import "gin-luban-server/model"

type SysOperationRecordSearch struct {
	model.SysOperationRecord
	PageInfo
	TimeRange
}

