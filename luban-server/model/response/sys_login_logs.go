package response

import "gin-luban-server/model"

type SysLoginLogsResponse struct {
	LoginLogs model.SysLoginLogs `json:"loginlogs"`
}
