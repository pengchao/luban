package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Response struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
	Msg  string      `json:"msg"`
}

const (
	ERROR   = 500
	SUCCESS = 0
	//表单验证返回码
	SUCCESS_VALIDATE = 1000
	ERROR_VALIDATE = 1001
	ERROR_NOT_USER_PASSWORD = 1002
	ERROR_VERIFICATION_CODE = 1003
	ERROR_TOKEN_FAILED = 1004


)


var codeMsg = map[int]string{
	SUCCESS:                 "操作成功",
	ERROR:                   "操作失败",
	SUCCESS_VALIDATE:        "ok",
	ERROR_NOT_USER_PASSWORD: "用户名不存在或者密码错误",
	ERROR_VERIFICATION_CODE: "验证码错误",
	ERROR_TOKEN_FAILED:      "获取token失败",
}

func GetErrMsg(code int) string {
	return codeMsg[code]
}


func Result(code int, data interface{}, msg string, c *gin.Context) {
	// 开始时间
	c.JSON(http.StatusOK, Response{
		code,
		data,
		msg,
	})
}

func Ok(code int,c *gin.Context) {
	Result(code, map[string]interface{}{}, GetErrMsg(code), c)
}

func OkWithMessage(message string, c *gin.Context) {
	Result(SUCCESS, map[string]interface{}{}, message, c)
}

func OkWithData(data interface{}, c *gin.Context) {
	Result(SUCCESS, data, GetErrMsg(SUCCESS), c)
}

func OkWithDetailed(data interface{}, message string, c *gin.Context) {
	Result(SUCCESS, data, message, c)
}

func Fail(code int,c *gin.Context) {
	Result(code, map[string]interface{}{}, GetErrMsg(code), c)
}
func FailWithMessage(message string, c *gin.Context) {
	Result(ERROR, map[string]interface{}{}, message, c)
}

func FailValidateMessage(message string, c *gin.Context) {
	Result(ERROR_VALIDATE, map[string]interface{}{}, message, c)
}

func FailWithDetailed(data interface{}, message string, c *gin.Context) {
	Result(ERROR, data, message, c)
}
