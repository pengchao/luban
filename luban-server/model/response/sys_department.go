package response

import "gin-luban-server/model"

type SysDepartmentResponse struct {
	Department model.SysDepartment `json:"department"`
}
