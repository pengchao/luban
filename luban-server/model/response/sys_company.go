package response

import "gin-luban-server/model"

type SysCompanyResponse struct {
	Company model.SysCompany `json:"company"`
}
