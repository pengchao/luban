package response

import "gin-luban-server/model"

type CMDBProjectResponse struct {
	Project model.CMDBProject `json:"project"`
}

