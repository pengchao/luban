package model

import (
	"gin-luban-server/global"
)

type SysBasicConfigure struct {
	global.GVA_MODEL
	BasicUser        string       `json:"basic_user" form:"basic_user" gorm:"column:basic_user;comment:用户;type:varchar(200);not null;size:200;"`
	SshType          string       `json:"sshType" form:"sshType" gorm:"column:sshType;comment:ssh类型;type:varchar(200);not null;size:200;"`
	BasicPasswd      string       `json:"basic_passwd" form:"basic_passwd" gorm:"column:basic_passwd;comment:密码;type:varchar(200);not null;size:200;"`
	SshKey           string       `json:"sshKey" form:"sshKey" gorm:"column:sshKey;comment:ssh密钥;type:text"`
	Purpose          string       `json:"purpose" form:"purpose" gorm:"column:purpose;comment:用途;type:varchar(200);not null;size:11;"`
	ProxyHost        string       `json:"proxy_host" form:"proxy_host" gorm:"column:proxy_host;comment:代理IP;type:varchar(200);not null;size:11;"`
	ProxyPort        string       `json:"proxy_port" form:"proxy_port" gorm:"column:proxy_port;comment:代理端口;type:varchar(200);not null;size:11;"`
	BaseUrl          string       `json:"base_url" form:"base_url" gorm:"column:base_url;comment:访问路径;type:varchar(200);not null;size:11;"`
	ServerIdc        string       `json:"server_idc" form:"server_idc" gorm:"column:server_idc;comment:环境;type:varchar(200);not null;size:11;"`
	Remark           string       `json:"remark" form:"remark" gorm:"column:remark;comment:备注;type:varchar(200);size:200;"`
	Status           *bool        `json:"status" form:"status" gorm:"column:status;comment:状态;type:tinyint;not null;"`
}
