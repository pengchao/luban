package model

import (
	"gin-luban-server/global"
)

type CmdbServer struct {
	global.GVA_MODEL
	Status       string `json:"status" form:"status" gorm:"column:status;comment:0-未初始化 1-已初始化;type:tinyint;not null;"`
	VirtualCode  string `json:"virtual_code" form:"virtual_code" gorm:"column:virtual_code;comment:虚拟号;type:varchar(200);not null;size:200;"`
	VirtualName  string `json:"virtual_name" form:"virtual_name" gorm:"column:virtual_name;comment:虚拟主机名;type:varchar(200);not null;size:200;"`
	ServerIdc    string `json:"server_idc" form:"server_idc" gorm:"column:server_idc;comment:主机的idc;type:varchar(200);not null;size:200;"`
	Ipaddress    string `json:"ip_address" form:"ip_address" gorm:"column:ip_address;comment:ip地址;type:varchar(200);not null;size:200;"`
	User         string `json:"user" form:"user" gorm:"column:user;comment:linux用户;type:varchar(200);not null;size:200;"`
	Password     string `json:"password" form:"password" gorm:"column:password;comment:linux密码;type:varchar(200);not null;size:200;"`
	Sshkey       string `json:"sshkey" form:"sshkey" gorm:"column:sshkey;comment:ssh的key;type:varchar(200);not null;size:200;"`
	Port         string `json:"port" form:"port" gorm:"column:port;comment:端口;type:int(11);not null;size:11;"`
	OsSystem     string `json:"os_system" form:"os_system" gorm:"column:os_system;comment:os系统;type:varchar(200);not null;size:200;"`
	OsType       string `json:"os_type" form:"os_type" gorm:"column:os_type;comment:系统的类型;type:varchar(200);not null;size:200;"`
	EnvName      string `json:"env_name" form:"env_name" gorm:"column:env_name;comment:环境名;type:varchar(200);not null;size:200;"`
	XShellProxy  string `json:"x_shell_proxy" form:"x_shell_proxy" gorm:"column:x_shell_proxy;comment:ssh代理;type:varchar(200);not null;size:200;"`
	CpuNum       string `json:"cpu_num" form:"cpu_num" gorm:"column:cpu_num;comment:cpu核数;type:varchar(11);not null;size:11;"`
	MemInfo      string `json:"mem_info" form:"mem_info" gorm:"column:mem_info;comment:内存;type:varchar(200);not null;size:200;"`
	ProjectCode  string `json:"project_code" form:"project_code" gorm:"column:project_code;type:comment:关联标记;type:varchar(255)"`
	Project      CMDBProject `json:"project"  gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
	DiskSpace    string `json:"disk_space" form:"disk_space" gorm:"column:disk_space;comment:磁盘;type:varchar(200);not null;size:200;"`
	CompanyCode  string `json:"company_code" form:"company_code" gorm:"column:company_code;comment:关联标记"`
	Company      SysCompany `json:"company"  gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司code"`
}
