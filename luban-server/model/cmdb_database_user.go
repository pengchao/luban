package model

import "gin-luban-server/global"

type CMDBDatabaseUser struct {
    global.GVA_MODEL
    ClusterName        string `json:"cluster_name" form:"cluster_name" gorm:"size:255;column:cluster_name;comment:集群名称"`
    DatabaseName       string `json:"database_name" form:"database_name"  gorm:"size:255;column:database_name;comment:数据库实例"`
    UserName           string `json:"user_name" form:"user_name"  gorm:"size:55;column:user_name;comment:用户名称"`
	Password           string `json:"password" form:"password" gorm:"size:255;column:password;comment:密码"`
	Permission         string `json:"permission" form:"permission" gorm:"size:255;column:permission;comment:权限"`
	ProjectCode        string `json:"project_code" form:"project_code" gorm:"column:project_code;type:comment:关联标记;type:varchar(255)"`
	Status             *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"`
	Project       CMDBProject `json:"project"  gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
    Database      CMDBDatabase`json:"database" gorm:"foreignKey:ClusterName;references:ClusterName;comment:集群名称"`
}
