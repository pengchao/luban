package model

import (
	"gin-luban-server/global"
)


type RedisCluster struct {
	global.GVA_MODEL
	Name   string `json:"name" form:"name" gorm:"column:name;type:varchar(128);not null;comment:集群名称"`
	Nodes    string `json:"nodes" form:"nodes"  gorm:"column:nodes;type:varchar(256);not null;comment:节点 逗号分隔"`
	Mode    string `json:"mode,omitempty" form:"mode"  gorm:"column:mode;type:varchar(1);not null;comment:redis类型 1.single  2.主从master-salve  3.哨兵 sentinel"`
	Status          *bool          `json:"status" form:"status" gorm:"comment:集群状态"`
	Env     string `json:"env" form:"env" gorm:"column:env;type:varchar(128);not null;comment:环境"`
	Password     string `json:"password" form:"env" gorm:"column:password;type:varchar(128);comment:密码"`
	XShellProxy  string `json:"x_shell_proxy" form:"x_shell_proxy" gorm:"column:x_shell_proxy;comment:ssh代理;type:varchar(200);not null;size:200;"`
	ClusterState string `json:"cluster_state"  gorm:"column:cluster_state;type:varchar(50)"`
	ClusterKnownNodes int `json:"cluster_known_nodes" gorm:"column:cluster_known_nodes;type:int(8)"`
	ClusterMaster string `json:"cluster_master"  gorm:"column:cluster_master;type:varchar(50)"`
	MaxDB int  `json:"max_db" gorm:"column:max_db;type:int(8);comment:最大db容量"`
	ProjectCode string `json:"project_code" form:"project_code" gorm:"column:project_code;comment:项目代码"`
	Project     CMDBProject `json:"project" gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
	*RedisNodeInfo  // 能连接
}


// 需要存入数据库节点中的采集的Redis 信息
type RedisNodeInfo struct {
	RedisVersion string `json:"redis_version" gorm:"column:redis_version;type:varchar(50);comment:版本"`
	RedisMode string `json:"redis_mode" gorm:"column:redis_mode;type:varchar(50);comment:模式"`
	OS string `json:"os" gorm:"column:os;type:varchar(50);comment:操作系统"`
	UptimeInSeconds string `json:"uptime_in_seconds" gorm:"column:uptime_in_seconds;type:varchar(50)"`
	TotalSystemMemoryHuman string  `json:"total_system_memory_human" gorm:"column:total_system_memory_human;type:varchar(50)"`
	UsedMemory string `json:"used_memory" gorm:"column:used_memory;type:varchar(50)"`
	DBSize int  `json:"db_size" gorm:"column:db_size;type:int(8)"`
	NodeRole    string `json:"node_role" gorm:"column:node_role;type:varchar(50);comment:节点角色"`
	TotalKeys int `json:"total_keys" gorm:"column:total_keys;type:int(8)"`
}


// 需要监控的其他信息
type RedisNodeOtherInfo struct {
	UsedMemoryPeak string `json:"used_memory_peak" gorm:"column:used_memory_peak;type:varchar(50)"`
	UsedMemoryRss string `json:"used_memory_rss" gorm:"column:used_memory_rss;type:varchar(50)"`
	ConnectedClients string `json:"connected_clients" gorm:"column:connected_clients;type:varchar(50)"`
	BlockedClients string `json:"blocked_clients" gorm:"column:blocked_clients;type:varchar(50)"`
	ExpiredKeys string `json:"expired_keys" gorm:"column:expired_keys;type:varchar(50)"`
	TotalCommandsProcessed string `json:"total_commands_processed" gorm:"column:total_commands_processed;type:varchar(50)"`
	KeySpaceHits string `json:"keyspace_hits" gorm:"column:keyspace_hits;type:varchar(50)"`
	KeySpaceMisses string `json:"keyspace_misses" gorm:"column:keyspace_misses;type:varchar(50)"`
}

type RedisNode struct {
	global.GVA_MODEL
	ClusterID uint `json:"cluster_id" gorm:"column:cluster_id;not null;comment:集群ID"`
	Node    string `json:"node" gorm:"column:node;type:varchar(50);comment:节点"`
	LinkState string `json:"link_state" gorm:"column:link_state;type:varchar(50);comment:redis状态"`
	*RedisNodeInfo  // 能连接 LinkState是好的 才有用
	RedisCluster  RedisCluster `json:"-" gorm:"foreignKey:cluster_id;references:id"`

}

// 定时监控的表 监控信息
type RedisMonitorInfo struct {
	RedisNode
	*RedisNodeOtherInfo
}



// monitor监控信息
type RedisMonitorQueryParams struct {
	ClusterID       uint `json:"cluster_id"`
	StartTime string `json:"start_time"`   // 时间戳格式 s 如: 1618472090
	EndTime string `json:"end_time"`       // 时间戳格式 s 如: 1618472090
	NodeList []string `json:"node_list"`
}


// scan 或者 query 都是这个model
type KeyScanParams struct {
	ClusterID       uint `json:"cluster_id"`
	Count int64 `json:"count"`   //
	Database int `json:"database"`       //
	Key string `json:"key"`
}

// redis 操作数据
type GETRedisCommand struct {
	ClusterID uint `json:"cluster_id" gorm:"column:cluster_id;not null;comment:集群ID"`
	Database    int `json:"database" gorm:"column:database;type:int(8);comment:选择的库"`
	Command string `json:"command" gorm:"column:command;type:varchar(50);comment:redis命令"`
}


//redis库号
/*
建议: 业务字段不用使用 int型 特别是有0 的，因为gorm中默认 整数型是0，在查找0的时候比较麻烦
*/
type RedisDatabaseRecord struct {
	global.GVA_MODEL
	RedisNumber string `json:"redis_number" form:"redis_number" gorm:"column:redis_number;type:varchar(4);not null;comment:库号"`
	Describe string `json:"describe" form:"describe" gorm:"column:describe;type:varchar(256);comment:描述信息"`
	ProjectCode string `json:"project_code" form:"project_code" gorm:"column:project_code;comment:项目代码"`
	Project     CMDBProject `json:"project" gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
	ClusterID uint `json:"cluster_id" gorm:"column:cluster_id;not null;comment:集群ID"`
	RedisCluster  RedisCluster `json:"redis" gorm:"foreignKey:cluster_id;references:id"`
}

type GetRedisDbListParams struct {
	ClusterID string `json:"cluster_id"`
}