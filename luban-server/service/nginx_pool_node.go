package service

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)

// 增
func CreateNginxPNode(PNode model.NginxPoolNode) (err error) {
	err = global.GVA_DB.Create(&PNode).Error
	return err
}

// 删
func DeleteNginxPNode(id float64) (err error) {
	var poolNode model.NginxPoolNode
	// 删除
	err = global.GVA_DB.Where("id = ?", id).Delete(&poolNode).Error
	return err
}

// 改
func UpdateNginxPNode(PNode *model.NginxPoolNode) (err error) {
	//err = global.GVA_DB.Save(PNode).Error
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", PNode.ID).First(&model.NginxPoolNode{}).Updates(&PNode).Error
	return err
}


// 查-id
func GetNginxPNodeById(id float64) (err error, poolNode model.NginxPoolNode) {
	err = global.GVA_DB.Where("id = ?", id).First(&poolNode).Error
	return err, poolNode
}

// 查-list
func GetNginxPNodeList(info request.NginxPoolNodeSearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.GVA_DB.Model(&model.NginxPoolNode{})
	var PNodes []model.NginxPoolNode
	// 如果有条件搜索 下方会自动创建搜索语句
	err = db.Count(&total).Error
	if info.ClusterID != 0 {
		err = db.Limit(limit).Offset(offset).Where("cluster_id = ?", info.ClusterID).Find(&PNodes).Error
	}
	return err, PNodes, total
}