package service

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)


// 增
func CreateNginxPool(Pool model.NginxPool) (err error) {
	err = global.GVA_DB.Create(&Pool).Error
	return err
}


// 删
func DeleteNginxPool(id float64) (err error) {
	var Pool model.NginxPool
	// 1. 判断pool是否被使用

	// 2. 删除
	err = global.GVA_DB.Where("id = ?", id).Delete(&Pool).Error
	return err
}


// 改
func UpdateNginxPool(Pool *model.NginxPool) (err error) {
	//err = global.GVA_DB.Save(Pool).Error  这种更新方式 有坑 要指定时间
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", Pool.ID).First(&model.NginxPool{}).Updates(&Pool).Error
	return err
}


// 查-id
func GetNginxPoolById(id float64) (err error, Pool model.NginxPool) {
	err = global.GVA_DB.Where("id = ?", id).First(&Pool).Error
	return err, Pool
}



// 查-list
func GetNginxPoolInfoList(info request.NginxPoolSearch, authId string) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
  // 创建db
	db := global.GVA_DB.Model(&model.NginxPool{})
  var Pools []model.NginxPool
  // 如果有条件搜索 下方会自动创建搜索语句
  if info.PoolName != "" {
     db = db.Where("`pool_name` LIKE ?","%"+ info.PoolName+"%")
  }
	err = db.Count(&total).Error
	err = db.Limit(limit).Offset(offset).Find(&Pools).Error
	return err, Pools, total
}
