package service

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)

//@author: heyibo
//@function: CreateJumpServerSshLogs
//@description: 创建堡垒机服务器
//@param: data JumpServerUser
//@return: err error, data model.JumpServerSshLogs

func CreateJumpServerSshLogs(logs model.JumpServerSshLogs)(err error)  {
	err = global.GVA_DB.Create(&logs).Error
	return err
}

//@author: heyibo
//@function: DeleteJumpServerSshLogs
//@description: 删除堡垒机服务器
//@param: id ,jumpUser model.JumpServerSshLogs
//@return: err error
func DeleteJumpServerSshLogs(id float64) (err error) {
	var logs model.JumpServerSshLogs
	db := global.GVA_DB.Where("id = ?", id).First(&logs)
	err = db.Delete(&logs).Error
	return err
}


//@author: heyibo
//@function: DeleteJumpServerSshLogsByIds
//@description: 批量删除记录
//@param: ids request.IdsReq
//@return: err error

func DeleteJumpServerSshLogsByIds(ids request.IdsReq) (err error) {
	err = global.GVA_DB.Delete(&[]model.JumpServerSshLogs{}, "id in (?)", ids.Ids).Error
	return err
}

//@author: heyibo
//@function: GetJumpServerSshLogsInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetJumpServerSshLogsInfoList(info request.SearchJumpServerSshLogsParams) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.JumpServerSshLogs{})
	var List []model.JumpServerSshLogs

	if info.UserName != "" {
		db = db.Where("user_name = ?", info.UserName)
	}
	if info.ClientIp != "" {
		db = db.Where("client_ip LIKE ?", "%"+info.ClientIp+"%")
	}
	err = db.Count(&total).Error

	if err != nil {
		return err, List, total
	} else {
		err = db.Limit(limit).Offset(offset).Find(&List).Error
	}
	return err, List, total
}

//@author: heyibo
//@function: UpdateJumpServerSshLogs
//@description: 更改一个角色
//@param: deployApply model.JumpServerSshLogs
//@return:err error, filter model.JumpServerSshLogs

func UpdateJumpServerSshLogs(id,status int) (err error) {
	var jumpLogs model.JumpServerSshLogs
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", id).First(&jumpLogs).Error
	jumpLogs.Status = status
	err = global.GVA_DB.Where("id = ?", id).Updates(jumpLogs).Error
	return err
}



