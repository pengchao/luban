package service

import (
	"errors"
	"fmt"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/utils"
	"gorm.io/gorm"
	"github.com/360EntSecGroup-Skylar/excelize/v2"

)

// 增
func CreateProject(project model.CMDBProject) (err error) {
	if !errors.Is(global.GVA_DB.Where("project_code = ? OR project_name = ?", project.ProjectCode, project.ProjectName).First(&model.CMDBProject{}).Error, gorm.ErrRecordNotFound) {
		return errors.New("存在相同项目名称！")
	}
	return global.GVA_DB.Create(&project).Error
}

// 删
func DeleteProject(id float64) (err error) {
	// 1. 判断不存在没有在其他 表中使用 虚拟机中 <- 待补充
	var project model.CMDBProject
	// 2. 删除
	err = global.GVA_DB.Where("id = ?", id).Delete(&project).Error
	return err
}

// 改
func UpdateProject(project model.CMDBProject) (err error) {
	var total int64
	var projects []model.CMDBProject
	err = global.GVA_DB.Where("project_code = ? AND id != ?",project.ProjectCode,project.ID).Find(&projects).Count(&total).Error
	if total >= 1 {
		return errors.New("存在相同项目名称！")
	}
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", project.ID).First(&model.CMDBProject{}).Updates(&project).Error
	return err
}

// 查
func FindProjectById(id float64) (err error, project model.CMDBProject) {
	err = global.GVA_DB.Where("id = ?", id).First(&project).Error
	return err, project
}

func GetCMDBProjectInfoList(project model.CMDBProject, info request.PageInfo, desc bool) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.CMDBProject{})
	var projectList []model.CMDBProject

	if project.ProjectName != "" {
		db = db.Where("project_name LIKE ?", "%"+project.ProjectName+"%")
	}

	if project.DevelopLanguage != "" {
		db = db.Where("develop_language = ?", project.DevelopLanguage)
	}

	if project.ProjectManager != "" {
		db = db.Where("project_manager = ?", project.ProjectManager)
	}

	if project.OperationsMaster != "" {
		db = db.Where("operations_master = ?", project.OperationsMaster)
	}

	if project.CompanyCode != "" {
		db = db.Where("company_code = ?", project.CompanyCode)
	}

	if project.Status != nil {
		db = db.Where("status = ?", project.Status)
	}

	err = db.Count(&total).Error

	if err != nil {
		return err, projectList, total
	} else {
		db = db.Limit(limit).Offset(offset)
		err = db.Preload("Company").Find(&projectList).Error
	}
	return err, projectList, total
}


func ParseProjectInfoList2Excel(project model.CMDBProject, filePath string) error {

	db := global.GVA_DB.Model(&model.CMDBProject{})
	var projectList []model.CMDBProject

	if project.ProjectName != "" {
		db = db.Where("project_name LIKE ?", "%"+project.ProjectName+"%")
	}

	if project.ProjectManager != "" {
		db = db.Where("project_manager = ?", project.ProjectManager)
	}

	if project.OperationsMaster != "" {
		db = db.Where("operations_master = ?", project.OperationsMaster)
	}

	if project.ProjectDevelop != "" {
		db = db.Where("project_develop = ?", project.ProjectDevelop)
	}

	err := db.Preload("Company").Find(&projectList).Error
	if err != nil {
		return err
	}
	// 写入 excel文件
	excel := excelize.NewFile()
	excel.SetSheetRow("Sheet1","A1",&[]string{"ID", "项目编号", "项目名称", "项目全称", "所属公司",
		"开发语言", "项目归属", "项目经理", "项目开发", "项目主运维", "项目备运维", "上线时间", "状态"})

	for i, menu := range projectList {
		axis := fmt.Sprintf("A%d",i+2)
		excel.SetSheetRow("Sheet1",axis,&[]interface{}{
			menu.ID,
			menu.ProjectCode,
			menu.ProjectName,
			menu.ProjectFullName,
			menu.Company.CompanyName,
			menu.DevelopLanguage,
			menu.ProjectAttribute,
			menu.ProjectManager,
			menu.ProjectDevelop,
			menu.OperationsMaster,
			menu.OperationsSlave,
			menu.OnlineTime.Format("2006-01-02"),   // 要转成字符串 写入excel
			utils.BoolToString(*menu.Status),      // 布尔值转成 字符串 正常|关闭
		})
	}
	excel.SaveAs(filePath)
	return nil
}


