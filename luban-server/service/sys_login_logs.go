package service

import (
	"fmt"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)

//@author: heyibo
//@function: CreateSysLoginLogs
//@description: 创建登陆记录
//@param: sysLoginlogs model.SysLoginLogs
//@return: err error

func CreateSysLoginLogs(sysLoginlogs model.SysLoginLogs) (err error) {
	err = global.GVA_DB.Create(&sysLoginlogs).Error
	return err
}

//@author: heyibo
//@function: GetSysLoginLogsInfoList
//@description: 分页获取操作记录列表
//@param: info request.SysLoginLogsSearch
//@return: err error, list interface{}, total int64

func GetSysLoginLogsInfoList(info request.SysLoginLogsSearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.GVA_DB.Model(&model.SysLoginLogs{})
	var sysLoginLogs []model.SysLoginLogs
	fmt.Println("ccc", info.Username)
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.Username != "" {
		db = db.Where("`username` LIKE ?", "%"+info.Username+"%")
	}
	if info.Ipaddr != "" {
		db = db.Where("`ipaddr` LIKE ?", "%"+info.Ipaddr+"%")
	}
	if info.Status != nil {
		db = db.Where("`status` = ?", info.Status)
	}
	err = db.Count(&total).Error
	err = db.Order("id desc").Limit(limit).Offset(offset).Find(&sysLoginLogs).Error
	return err, sysLoginLogs, total
}

//@author: heyibo
//@function: DeleteSysLoginLogsByIds
//@description: 批量删除登陆记录
//@param: ids request.IdsReq
//@return: err error

func DeleteSysLoginLogsByIds(ids request.IdsReq) (err error) {
	err = global.GVA_DB.Delete(&[]model.SysLoginLogs{}, "id in (?)", ids.Ids).Error
	return err
}

//@author: heyibo
//@function: DeleteSysLoginLogs
//@description: 删除登陆记录
//@param: sysLoginLogs model.SysLoginLogs
//@return: err error

func DeleteSysLoginLogs(sysLoginLogs model.SysLoginLogs) (err error) {
	err = global.GVA_DB.Delete(sysLoginLogs).Error
	return err
}

//@author: heyibo
//@function: GetLoginLogsById
//@description: 通过id获取公司信息
//@param: id int
//@return: err error, loginlogs *model.SysLoginLogs

func GetLoginLogsById(id float64) (err error, loginlogs model.SysLoginLogs) {
	err = global.GVA_DB.Where("id = ?", id).First(&loginlogs).Error
	return err, loginlogs
}
