package service

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)


func DeleteCmdbSecurity(cmdbSecurityDEL model.CMDBSecurity) (err error) {
	err = global.GVA_DB.Delete(&cmdbSecurityDEL).Error
	return err
}
//update
func UpdateCmdbSecurity(cmdbsecupdate model.CMDBSecurity) (err error) {
	//err = global.GVA_DB.Save(&cmdbsecupdate).Error
	err = global.GVA_DB.Where("id = ?", cmdbsecupdate.ID).First(&model.CMDBSecurity{}).Updates(&cmdbsecupdate).Error
	return err
}
//create
func CreateCmdbSecuritys(cmdbSeccreate model.CMDBSecurity) (err error) {
	err = global.GVA_DB.Create(&cmdbSeccreate).Error
	return err
}
//get
func GetCMDBSecurityInfoList(cmdbsec model.CMDBSecurity,info request.PageInfo,desc bool) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.GVA_DB.Model(&model.CMDBSecurity{})
	var cmdbSecurity []model.CMDBSecurity
	// 如果有条件搜索 下方会自动创建搜索语句
	if cmdbsec.VirtualCode != "" {
		db = db.Where("virtual_code = ?",cmdbsec.VirtualCode)
	}
	if cmdbsec.Uput != "" {
		db = db.Where(" uput LIKE ?","%"+ cmdbsec.Uput+"%")
	}

	if cmdbsec.Status != nil {
		db = db.Where("status = ?", cmdbsec.Status)
	}
	err = db.Count(&total).Error

	if err != nil {
		return err,cmdbSecurity, total
	} else {
		db = db.Limit(limit).Offset(offset)
		err = db.Preload("CServer").Find(&cmdbSecurity).Error
	}
	return err, cmdbSecurity, total
}
