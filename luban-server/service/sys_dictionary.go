package service

import (
	"errors"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gorm.io/gorm"
)

//@author: heyibo
//@function: DeleteSysDictionary
//@description: 创建字典数据
//@param: sysDictionary model.SysDictionary
//@return: err error

func CreateSysDictionary(sysDictionary model.SysDictionary) (err error) {
	if (!errors.Is(global.GVA_DB.First(&model.SysDictionary{}, "dict_code = ?", sysDictionary.DictCode).Error, gorm.ErrRecordNotFound)) {
		return errors.New("存在相同的code，不允许创建")
	}
	err = global.GVA_DB.Create(&sysDictionary).Error
	return err
}

//@author: heyibo
//@function: DeleteSysDictionary
//@description: 删除字典数据
//@param: sysDictionary model.SysDictionary
//@return: err error

func DeleteSysDictionary(sysDictionary *model.SysDictionary) (err error) {
	db := global.GVA_DB.Preload("SysDictionaryDetails").First(sysDictionary)
	if len(sysDictionary.SysDictionaryDetails) > 0 {
		return errors.New("此字典有键值禁止删除")
	}
	err = db.Delete(&sysDictionary).Error
	return err
}

//@author: heyibo
//@function: UpdateSysDictionary
//@description: 更新字典数据
//@param: sysDictionary *model.SysDictionary
//@return: err error

func UpdateSysDictionary(sysDictionary *model.SysDictionary) (err error) {
	var dict model.SysDictionary
	sysDictionaryMap := map[string]interface{}{
		"DictName":   sysDictionary.DictName,
		"DictCode":   sysDictionary.DictCode,
		"Status": sysDictionary.Status,
		"DictDesc":   sysDictionary.DictDesc,
	}
	db := global.GVA_DB.Where("id = ?", sysDictionary.ID).First(&dict)
	if dict.DictCode == sysDictionary.DictCode {
		err = db.Updates(sysDictionaryMap).Error
	} else {
		if (!errors.Is(global.GVA_DB.First(&model.SysDictionary{}, "dict_code = ?", sysDictionary.DictCode).Error, gorm.ErrRecordNotFound)) {
			return errors.New("存在相同的字典编码，不允许创建")
		}
		err = db.Updates(sysDictionaryMap).Error

	}
	return err
}

//@author: heyibo
//@function: GetSysDictionary
//@description: 根据id或者type获取字典单条数据
//@param: Type string, Id uint
//@return: err error, sysDictionary model.SysDictionary

func GetSysDictionaryById( Id float64) (err error, sysDictionary model.SysDictionary) {
	err = global.GVA_DB.Model(&model.SysDictionary{}).Where("id = ?", Id).Preload("SysDictionaryDetails").First(&sysDictionary).Error
	return
}

//@author: heyibo
//@function: GetSysDictionary
//@description: 根据id或者type获取字典单条数据
//@param: Type string, Id uint
//@return: err error, sysDictionary model.SysDictionary

func GetSysDictionary() (err error, sysDictionary []model.SysDictionary) {
	err = global.GVA_DB.Find(&sysDictionary).Error
	return
}


//@author: heyibo
//@function: GetSysDictionaryInfoList
//@description: 分页获取字典列表
//@param: info request.SysDictionarySearch
//@return: err error, list interface{}, total int64

func GetSysDictionaryInfoList(info request.SysDictionarySearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	// 创建db
	db := global.GVA_DB.Model(&model.SysDictionary{})
	var sysDictionarys []model.SysDictionary
	// 如果有条件搜索 下方会自动创建搜索语句
	if info.DictName != "" {
		db = db.Where("`dict_name` LIKE ?", "%"+info.DictName+"%")
	}
	if info.DictCode != "" {
		db = db.Where("`dict_code` LIKE ?", "%"+info.DictCode+"%")
	}
	if info.Status != nil {
		db = db.Where("`status` = ?", info.Status)
	}
	err = db.Count(&total).Error
	err = db.Limit(limit).Offset(offset).Preload("SysDictionaryDetails").Find(&sysDictionarys).Error
	return err, sysDictionarys, total
}
