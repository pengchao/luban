package datas

import (
	"gin-luban-server/global"
	"github.com/gookit/color"
	"os"
	"time"

	"gin-luban-server/model"
	"gorm.io/gorm"
)

func InitSysDictionary(db *gorm.DB) {
	var status = new(bool)
	*status = true
	Dictionaries := []model.SysDictionary{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sys_common_status", DictName: "系统状态",DictDesc: "系统状态列表",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 2, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sys_user_sex", DictName: "用户性别",DictDesc: "用户性别列表",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 3, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "programming_language", DictName: "编程语言",DictDesc: "编程语言",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 4, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "project_attribute", DictName: "项目属性",DictDesc: "自研或外采",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 5, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "env_name", DictName: "环境名称",DictDesc: "部署有几个环境",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 6, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_type", DictName: "系统类型",DictDesc: "服务器操作系统的类型",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 7, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_system", DictName: "系统名称",DictDesc: "系统名称",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 8, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "cpu_num", DictName: "cpu核数",DictDesc: "cpu核数",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 9, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info", DictName: "内存大小",DictDesc: "内存大小",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 10, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space", DictName: "磁盘大小",DictDesc: "磁盘大小",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 11, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "server_idc", DictName: "机房名称",DictDesc: "机房名称",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 12, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "system_user", DictName: "系统用户",DictDesc: "虚拟主机系统用户",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 13, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_type", DictName: "数据库类型",DictDesc: "数据库类型",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 14, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "redis_mode", DictName: "redis模式",DictDesc: "redis模式 单机、主从、哨兵",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 15, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "security_put", DictName: "流量方向",DictDesc: "流量进出方向",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 16, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sec_protocol", DictName: "协议类型",DictDesc: "协议类型协议类型",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 17, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "apps_type", DictName: "应用类型",DictDesc: "应用类型",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 18, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "deploy_status", DictName: "部署状态",DictDesc: "部署状态",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 19, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage", DictName: "用途",DictDesc: "基础工具用途",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 20, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "build_status", DictName: "构建状态",DictDesc: "jenkins构建的状态",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 21, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status", DictName: "审计状态",DictDesc: "堡垒机日志审计状态",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 22, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_proxy", DictName: "跳板机",DictDesc: "跳板机",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 23, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_type", DictName: "连接类型",DictDesc: "ssh连接类型",Status: status},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1, 23}).Find(&[]model.SysDictionary{}).RowsAffected == 2 {
			color.Danger.Println("sys_dictionaries表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Dictionaries).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_dictionaries 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}