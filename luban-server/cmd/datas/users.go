package datas

import (
	"gin-luban-server/global"
	"github.com/gookit/color"
	"os"
	"time"

	"gin-luban-server/model"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

func InitSysUser(db *gorm.DB) {
	status := new(bool)
	*status = true
	var Users = []model.SysUser{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()}, UUID: uuid.NewV4(), Username: "admin", Password: "e10adc3949ba59abbe56e057f20f883e", NickName: "超级管理员", HeaderImg: "http://qmplusimg.henrongyi.top/gva_header.jpg",Email:"admin@anji-plus.com",Status: status,Sex: "1",DeptCode: "100001",CompanyCode: "100001",Phone: "10016250000",StationCode: "100001",AuthorityId: "admin",Remark: "超级管理员",DeployProd: true},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1}).Find(&[]model.SysUser{}).RowsAffected == 2 {
			color.Danger.Println("sys_users表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Users).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_users 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
