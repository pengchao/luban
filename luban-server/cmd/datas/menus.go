package datas

import (
	"gin-luban-server/global"
	"github.com/gookit/color"
	"os"
	"time"

	"gin-luban-server/model"
	"gorm.io/gorm"
)

var BaseMenus = []model.SysBaseMenu{
	{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false,ParentId: "0", Path: "dashboard", Name: "dashboard", Component: "view/dashboard/index.vue", Sort: 1, Meta: model.Meta{Title: "仪表盘", Icon: "setting"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 2, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "admin", Name: "superAdmin", Component: "view/superAdmin/index.vue", Sort: 2, Meta: model.Meta{Title: "系统管理", Icon: "user-solid"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 3, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "0", Path: "person", Name: "person", Component: "view/person/person.vue", Sort: 3, Meta: model.Meta{Title: "个人信息", Icon: "message-solid"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 4, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "authority", Name: "authority", Component: "view/superAdmin/authority/authority.vue", Sort: 1, Meta: model.Meta{Title: "角色管理", Icon: "s-custom"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 5, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "menu", Name: "menu", Component: "view/superAdmin/menu/menu.vue", Sort: 2, Meta: model.Meta{Title: "菜单管理", Icon: "s-order", KeepAlive: true}},
	{GVA_MODEL: global.GVA_MODEL{ID: 6, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "api", Name: "api", Component: "view/superAdmin/api/api.vue", Sort: 3, Meta: model.Meta{Title: "api管理", Icon: "s-platform", KeepAlive: true}},
	{GVA_MODEL: global.GVA_MODEL{ID: 7, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "user", Name: "user", Component: "view/superAdmin/user/user.vue", Sort: 4, Meta: model.Meta{Title: "用户管理", Icon: "coordinate"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 8, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "dictionary", Name: "dictionary", Component: "view/superAdmin/dictionary/sysDictionary.vue", Sort: 5, Meta: model.Meta{Title: "字典管理", Icon: "notebook-2"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 9, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "2", Path: "dictionaryDetail/:id", Name: "dictionaryDetail", Component: "view/superAdmin/dictionary/sysDictionaryDetail.vue", Sort: 6, Meta: model.Meta{Title: "字典详情", Icon: "s-order"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 10, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "company", Name: "company", Component: "view/superAdmin/company/company.vue", Sort: 7, Meta: model.Meta{Title: "公司管理", Icon: "office-building"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 11, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "department", Name: "department", Component: "view/superAdmin/department/department.vue", Sort: 8, Meta: model.Meta{Title: "部门管理", Icon: "school"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 12, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "station", Name: "station", Component: "view/superAdmin/station/station.vue", Sort: 9, Meta: model.Meta{Title: "岗位管理", Icon: "s-custom"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 13, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "basicConfigure", Name: "basiConfigure", Component: "view/superAdmin/basicConfigure/basicConfigure.vue", Sort: 10, Meta: model.Meta{Title: "基础配置", Icon: "reading"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 14, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "2", Path: "logs", Name: "logs", Component: "view/superAdmin/logs/index.vue", Sort: 11, Meta: model.Meta{Title: "日志管理", Icon: "notebook-2"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 15, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "14", Path: "loginLogs", Name: "loginLogs", Component: "view/superAdmin/logs/loginLogs/loginLogs.vue", Sort: 12, Meta: model.Meta{Title: "登陆日志", Icon: "s-check"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 16, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "14", Path: "operation", Name: "operation", Component: "view/superAdmin/logs/operation/sysOperationRecord.vue", Sort: 13, Meta: model.Meta{Title: "操作历史", Icon: "time"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 17, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "cmdb", Name: "cmdb", Component: "view/cmdb/index.vue", Sort: 4, Meta: model.Meta{Title: "资产管理", Icon: "s-open"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 18, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "17", Path: "project", Name: "project", Component: "view/cmdb/project/project.vue", Sort: 1, Meta: model.Meta{Title: "项目管理", Icon: "goods"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 19, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "projectDetail", Name: "projectDetail", Component: "view/cmdb/project/projectDetail.vue", Sort: 2, Meta: model.Meta{Title: "项目详情", Icon: "info"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 20, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "17", Path: "virtual", Name: "virtual", Component: "view/cmdb/virtualMachine/virtualMachine.vue", Sort: 3, Meta: model.Meta{Title: "虚拟机管理", Icon: "s-platform"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 21, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "virtualDetail", Name: "virtualDetail", Component: "view/cmdb/virtualMachine/virtualDetail.vue", Sort: 4, Meta: model.Meta{Title: "虚拟机详情", Icon: "s-platform"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 22, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "virtualSecurity/:id", Name: "virtualSecurity", Component: "view/cmdb/virtualMachine/virtualSecurity.vue", Sort: 5, Meta: model.Meta{Title: "虚拟机防火墙", Icon: "info"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 23, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "17", Path: "database", Name: "database", Component: "view/cmdb/database/database.vue", Sort: 6, Meta: model.Meta{Title: "数据库管理", Icon: "data-analysis"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 24, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "databaseDetail", Name: "databaseDetail", Component: "view/cmdb/database/databaseDetail.vue", Sort: 7, Meta: model.Meta{Title: "数据库详情", Icon: "data-line"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 25, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "databaseUsers", Name: "databaseUsers", Component: "view/cmdb/database/databaseUsers.vue", Sort: 8, Meta: model.Meta{Title: "数据库账号管理", Icon: "platform-eleme"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 26, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "17", Path: "domain", Name: "domain", Component: "view/cmdb/domain/domain.vue", Sort: 9, Meta: model.Meta{Title: "域名管理", Icon: "odometer"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 27, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "domainDetail", Name: "domainDetail", Component: "view/cmdb/domain/domainDetail.vue", Sort: 10, Meta: model.Meta{Title: "域名详情", Icon: "info"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 28, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "17", Path: "redis", Name: "redis", Component: "view/cmdb/redis/redis.vue", Sort: 11, Meta: model.Meta{Title: "缓存管理", Icon: "orange"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 29, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "redisDetail", Name: "redisDetail", Component: "view/cmdb/redis/redisDetail.vue", Sort: 12, Meta: model.Meta{Title: "redis详情", Icon: "info"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 30, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "17", Path: "redisDatabase", Name: "redisDatabase", Component: "view/cmdb/redis/redisDatabase.vue", Sort: 13, Meta: model.Meta{Title: "redis库号", Icon: "info"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 31, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "jump", Name: "jump", Component: "view/jump/index.vue", Sort: 5, Meta: model.Meta{Title: "堡垒机", Icon: "monitor"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 32, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "31", Path: "jumpServerUser", Name: "jumpServerUser", Component: "view/jump/user/jumpServerUser.vue", Sort: 1, Meta: model.Meta{Title: "我的服务器", Icon: "setting"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 33, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "31", Path: "jumpServerFilter", Name: "jumpServerFilter", Component: "view/jump/filter/jumpServerFilter.vue", Sort: 2, Meta: model.Meta{Title: "过滤规则", Icon: "s-order"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 34, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "31", Path: "jumpServerLogs", Name: "jumpServerLogs", Component: "view/jump/logs/jumpServerLogs.vue", Sort: 3, Meta: model.Meta{Title: "审计日志", Icon: "chat-line-square"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 35, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "deploy", Name: "deploy", Component: "view/deploy/index.vue", Sort: 6, Meta: model.Meta{Title: "部署管理", Icon: "s-tools"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 36, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "35", Path: "configureIndex", Name: "configureIndex", Component: "view/deploy/configure/index.vue", Sort: 1, Meta: model.Meta{Title: "应用配置", Icon: "s-comment"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 37, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "35", Path: "releaseIndex", Name: "releaseIndex", Component: "view/deploy/release/index.vue", Sort: 2, Meta: model.Meta{Title: "应用部署", Icon: "set-up"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 38, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "35", Path: "apply", Name: "apply", Component: "view/deploy/configure/apply.vue", Sort: 3, Meta: model.Meta{Title: "添加应用页面", Icon: "s-grid"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 39, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "35", Path: "configure", Name: "configure", Component: "view/deploy/configure/configure.vue", Sort: 4, Meta: model.Meta{Title: "应用配置页面", Icon: "platform-eleme"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 40, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: true, ParentId: "35", Path: "appList", Name: "appList", Component: "view/deploy/release/appList.vue", Sort: 5, Meta: model.Meta{Title: "项目应用发布列表", Icon: "more"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 41, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "35", Path: "history", Name: "history", Component: "view/deploy/history/historyList.vue", Sort: 6, Meta: model.Meta{Title: "发布历史", Icon: "more"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 42, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "35", Path: "rollback", Name: "rollback", Component: "view/deploy/rollback/rollback.vue", Sort: 7, Meta: model.Meta{Title: "应用回滚", Icon: "s-promotion"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 43, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "35", Path: "restart", Name: "restart", Component: "view/deploy/restart/restart.vue", Sort: 8, Meta: model.Meta{Title: "应用重启", Icon: "star-on"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 44, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "redisAdmin", Name: "redisAdmin", Component: "view/redisAdmin/index.vue", Sort: 7, Meta: model.Meta{Title: "REDIS管理", Icon: "picture-outline-round"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 45, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "44", Path: "redisDashboard", Name: "redisDashboard", Component: "view/redisAdmin/redisDashboard/dashboard.vue", Sort: 1, Meta: model.Meta{Title: "集群信息", Icon: "s-platform"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 46, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "44", Path: "redisOperation", Name: "redisOperation", Component: "view/redisAdmin/redisOperation/operation.vue", Sort: 2, Meta: model.Meta{Title: "终端操作", Icon: "open"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 47, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "ansible", Name: "ansible", Component: "view/ansible/index.vue", Sort: 8, Meta: model.Meta{Title: "运维操作", Icon: "edit-outline"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 48, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "47", Path: "initialization", Name: "initialization", Component: "view/ansible/initialization/initialization.vue", Sort: 1, Meta: model.Meta{Title: "主机初始化", Icon: "cpu"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 49, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "state", Name: "state", Component: "view/system/state.vue", Sort: 9, Meta: model.Meta{Title: "服务器状态", Icon: "cloudy"}},
	{GVA_MODEL: global.GVA_MODEL{ID: 50, CreatedAt: time.Now(), UpdatedAt: time.Now()}, MenuLevel: 0, Hidden: false, ParentId: "0", Path: "about", Name: "about", Component: "view/about/index.vue", Sort: 10, Meta: model.Meta{Title: "关于我们", Icon: "info"}},
}

func InitSysBaseMenus(db *gorm.DB) {
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1, 27}).Find(&[]model.SysBaseMenu{}).RowsAffected == 2 {
			color.Danger.Println("sys_base_menus表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&BaseMenus).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_base_menus 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
