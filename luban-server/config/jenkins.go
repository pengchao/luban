package config

type Jenkins struct {
	RollbackJavaSpringJob string `mapstructure:"rollback-java-spring-job" json:"rollback-java-spring-job" yaml:"rollback-java-spring-job"`
	RollbackVuePortalJob string `mapstructure:"rollback-vue-portal-job" json:"rollback-vue-portal-job" yaml:"rollback-vue-portal-job"`
}

type Deploy struct {
	Timeout int64 `mapstructure:"timeout" json:"timeout" yaml:"timeout"`
	ExcludeTimeoutJobs []string `mapstructure:"exclude-timeout-jobs" json:"exclude-timeout-jobs" yaml:"exclude-timeout-jobs"`
}

type FTPPackage struct {
	Telecom string `mapstructure:"telecom" json:"telecom" yaml:"telecom"`
	ShangKai9 string `mapstructure:"shang-kai-9" json:" shang-kai-9" yaml:"shang-kai-9"`
	SaicCloud string `mapstructure:"saic-cloud" json:" saic-cloud" yaml:"saic-cloud"`
}

