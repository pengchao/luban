package utils

var (
	IdVerify               = Rules{"ID": {NotEmpty()}}
	ApiVerify              = Rules{"Path": {NotEmpty()}, "Description": {NotEmpty()}, "ApiGroup": {NotEmpty()}, "Method": {NotEmpty()}}
	MenuVerify             = Rules{"Path": {NotEmpty()}, "ParentId": {NotEmpty()}, "Name": {NotEmpty()}, "Component": {NotEmpty()}, "Sort": {Ge("0")}}
	MenuMetaVerify         = Rules{"Title": {NotEmpty()}}
	LoginVerify            = Rules{"CaptchaId": {NotEmpty()}, "Captcha": {NotEmpty()}, "Username": {NotEmpty()}, "Password": {NotEmpty()}}
	SetUserVerify          = Rules{"ID": {NotEmpty()}, "Username": {NotEmpty()}, "NickName": {NotEmpty()}, "HeaderImg": {NotEmpty()}}
	RegisterVerify         = Rules{"Username": {NotEmpty()}, "NickName": {NotEmpty()}, "Password": {NotEmpty()}, "AuthorityId": {NotEmpty()}}
	PageInfoVerify         = Rules{"Page": {NotEmpty()}, "PageSize": {NotEmpty()}}
	AuthorityVerify        = Rules{"AuthorityId": {NotEmpty()}, "AuthorityName": {NotEmpty()}, "ParentId": {NotEmpty()}}
	AuthorityIdVerify      = Rules{"AuthorityId": {NotEmpty()}}
	OldAuthorityVerify     = Rules{"OldAuthorityId": {NotEmpty()}}
	ChangePasswordVerify   = Rules{"Username": {NotEmpty()}, "Password": {NotEmpty()}, "NewPassword": {NotEmpty()}}
	SetUserAuthorityVerify = Rules{"UUID": {NotEmpty()}, "AuthorityId": {NotEmpty()}}

	// CMDB
	ProjectVerify          = Rules{"ProjectCode": {NotEmpty()}, "ProjectName": {NotEmpty()}, "ProjectFullName": {NotEmpty()}, "ProjectManager": {NotEmpty()},
								"ProjectDevelop": {NotEmpty()}, "OperationsMaster": {NotEmpty()},"OperationsSlave": {NotEmpty()},
		"CompanyCode": {NotEmpty()}, "DevelopLanguage": {NotEmpty()}, "Status": {NotEmpty()}}
	DomainVerify          = Rules{"DomainName": {NotEmpty()},  "Protocol": {NotEmpty()}, "DomainIndex": {NotEmpty()}, "Status": {NotEmpty()}, "Env":{NotEmpty()}, "ProjectCode": {NotEmpty()},"ExternalPort":{NotEmpty()},"InternalPort":{NotEmpty()} }
	RedisVerify          = Rules{"Name": {NotEmpty()},  "Nodes": {NotEmpty()}, "Status": {NotEmpty()}, "Env":{NotEmpty()}, "Database": {NotEmpty()} }
	RedisDatabaseVerify          = Rules{"RedisID": {NotEmpty()}}
)
