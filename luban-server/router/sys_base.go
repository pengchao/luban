package router

import (
	"gin-luban-server/api/v1"
	"github.com/gin-gonic/gin"
)

func InitBaseRouter(Router *gin.RouterGroup) (R gin.IRoutes) {
	BaseRouter := Router.Group("base")
	{
		BaseRouter.POST("login", v1.Login)
		BaseRouter.POST("captcha", v1.Captcha)

		// dns解析功能
		BaseRouter.GET("resolverIP", v1.ResolveIP)

		// 发布jenkins回调的通知路由
		BaseRouter.GET("deploy/notify", v1.DeployNotify)
		BaseRouter.GET("/deploy/jenkins/getJenkinsBuildJobLogsWS", v1.GetJenkinsBuildJobLogsWS) //查询日志 WS

	}
	return BaseRouter
}
