package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitDeployAppConfigureRouter(Router *gin.RouterGroup) {
	DeployAppConfigureRouter := Router.Group("dispose").Use(middleware.OperationRecord())
	{
		DeployAppConfigureRouter.POST("createAppConfigure", v1.CreateAppConfigure)  //新增
		DeployAppConfigureRouter.PUT("updateAppConfigure", v1.UpdateAppConfigure)   //更新
		DeployAppConfigureRouter.DELETE("deleteAppConfigure", v1.DeleteAppConfigure)//删除同Jnekins
		DeployAppConfigureRouter.GET("getAppConfigureList", v1.GetAppConfigureList) //查询
		DeployAppConfigureRouter.POST("copyAppConfigure", v1.CopyAppConfigure)  //获取DeployApp列表
	}
}
