package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCMDBProjectRouter(Router *gin.RouterGroup) {
	CMDBProjectRouter := Router.Group("project").Use(middleware.OperationRecord())
	{
		CMDBProjectRouter.POST("createProject", v1.CreateProject)   // 添加CMDB项目
		CMDBProjectRouter.PUT("updateProject", v1.UpdateProject)   // 更新CMDB项目
		CMDBProjectRouter.POST("getProjectById", v1.GetProjectById)  // 通过ID获取项目信息
		CMDBProjectRouter.DELETE("deleteProject", v1.DeleteProject)  // 通过ID删除项目
		CMDBProjectRouter.POST("getProjectList", v1.GetCMDBProjectList) // 获取项目列表
		CMDBProjectRouter.GET("exportExcel", v1.ProjectExportExcel)     // 导出excel列表
	}
}


