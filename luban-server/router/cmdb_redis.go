package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCMDBRedisRouter(Router *gin.RouterGroup) {
	CMDBRedisRouter := Router.Group("redis").Use(middleware.OperationRecord())
	{
		CMDBRedisRouter.POST("createCluster", v1.AddRedisCluster)
		CMDBRedisRouter.POST("getClusterList", v1.GetRedisClusterList)
		CMDBRedisRouter.DELETE("deleteCluster", v1.DeleteRedisCluster)
		CMDBRedisRouter.POST("getClusterById", v1.GetRedisClusterById)
		CMDBRedisRouter.PUT("updateCluster", v1.UpdateRedisCluster)
		// 导出excel
		CMDBRedisRouter.GET("exportExcel", v1.RedisExportExcel)     // 导出excel列表
		// redis 库号表
		CMDBRedisRouter.POST("createRedisDatabase", v1.CreateRedisDatabase)   // 添加CMDB Redis Database列表
		CMDBRedisRouter.DELETE("deleteRedisDatabase", v1.DeleteRedisDatabase)  // 通过ID删除域名
		CMDBRedisRouter.PUT("updateRedisDatabase", v1.UpdateRedisDatabase)   // 更新CMDB Redis Database列表
		CMDBRedisRouter.POST("getRedisDatabaseList", v1.GetRedisDatabaseList) // 获取Redis Database列表
		// 监控数据路由
		CMDBRedisRouter.POST("monitor/getInfoItemMonitorData", v1.GetInfoItemMonitorData)
		// 接受命令操作
		CMDBRedisRouter.POST("command/sendCommand", v1.SendCommand)
		// redis query key 或者 scan key
		CMDBRedisRouter.POST("key/scan", v1.KeyScan)
		CMDBRedisRouter.POST("key/query", v1.KeyQuery)
		// 获取集群下的dbLists
		CMDBRedisRouter.GET("data/getDBList/:cluster_id", v1.GetDBList)
	}
}


