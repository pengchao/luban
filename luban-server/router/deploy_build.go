package router

import (
	v1 "gin-luban-server/api/v1"
	"github.com/gin-gonic/gin"
)

func InitDeployBuild(Router *gin.RouterGroup) {
	DeployAppRouter := Router.Group("build")
	{
		DeployAppRouter.POST("getDeployBuildList", v1.GetDeployBuildList) //查询
		//DeployAppRouter.GET("getDeployAppBranchList",v1.GetDeployAppBranchList) //查询分支接口
		DeployAppRouter.GET("getAppGitRepoInfo",v1.GetAppGitRepoInfo) //查询分支、Tag接口
		DeployAppRouter.GET("getAppVirtualList",v1.GetAppVirtualList)  //查询部署服务器列表
	}
}
