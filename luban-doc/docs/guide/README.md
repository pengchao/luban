&emsp; &emsp; AJ-luban是[安吉加加](https://gitee.com/link?target=http%3A%2F%2Fwww.anji-plus.com) 开源的运维平台，集成 cmdb管理、CICD，堡垒机等运维常用功能模块，界面简洁优雅易用，提供鲁班系统的前后端源码，帮您快速构建企业平台运维功能、完善运维流程。 鲁班完全开源，有详尽的开发和操作文档，我们将不遗余力，丰富鲁班功能，期待大家的使用并提出宝贵意见。

## 系统特性

1. 最新最稳定的技术栈；
2. 支持多云、多机房运维
3. 能够按照项目配置相应权限

## 在线体验

#### &emsp; [在线体验](https://uat.luban.anji-plus.com/index.html "链接"): https://uat.luban.anji-plus.com/index.html  &emsp;体验账号：admin 密码：123456

#### &emsp; [在线文档](https://luban.anji-plus.com/luban-doc "doc"): https://luban.anji-plus.com/luban-doc <br>

#### &emsp; [在线提问](https://gitee.com/anji-plus/luban/issues "issue"): https://gitee.com/anji-plus/luban/issues <br>

## 发行版本

#### &emsp; [下载链接](https://gitee.com/anji-plus/luban/releases "下载链接"): https://gitee.com/anji-plus/luban/releases <br>

## 功能概述

#### &emsp; 组件介绍

&emsp;&emsp; 大屏设计（AJ-Report）是一个可视化拖拽编辑的，直观，酷炫，具有科技感的图表工具全开源项目。 内置的基础功能包括数据源，数据集，报表管理，项目部分截图如下。<br>

![操作](https://images.gitee.com/uploads/images/2021/0703/094742_c0243f70_1728982.gif "2021-07-03_09-43-50.gif")

![视频](https://report.anji-plus.com/report-doc/static/Rhea.mp4) <br>

**[更多社区大屏案例](https://report.anji-plus.com/report-doc/guide/bigScreenCase.html)** <br>

## 数据流程图

![An image](https://images.gitee.com/uploads/images/2021/0630/160451_31bb9052_1728982.png)

## 打包目录

```
├── bin                                           启动命令脚本
│   ├── restart.sh
│   ├── start.bat
│   ├── start.sh
│   └── stop.sh
├── conf                                       配置文件目录
│   └── bootstrap-dev.yml
├── logs                                          启动日志目录
├── cache                                         本地缓存目录
├── lib                                           自定义扩展包&report-core核心包
```

## 系统目录

```
├── doc                                           文档源码
│   ├── docs
│   ├── package.json
│   └── README.md
├── pom.xml                                       父pom，jar版本管理
├── report-core                                   java源码
│   ├── pom.xml                                   gaea父pom，jar版本管理
│   └── README.md
├── report-ui                                     前端vue源码
├── LICENSE
├── README.md
```




