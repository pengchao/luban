import service from '@/utils/request'

// 解析dns域名接口
export const resolverIP = (params) => {
  return service({
    url: "/base/resolverIP",
    method: 'get',
    params
  })
}