import service from '@/utils/request'
import { handleFileError } from '@/utils/download'
// @Tags server
// @Summary 分页虚拟主机色列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页获取server列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/GetServerList [get]
// {
//  page     int
//	pageSize int
// }
export const getServerList = (params) => {
  return service({
    url: "/cmdb/server/getServerList",
    method: 'get',
    params
  })
}

// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "创建server列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/createServer [post]

export const createServer = (data) => {
  return service({
    url: "/cmdb/server/createServer",
    method: 'post',
    data
  })
}

// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "删除server"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/deleteServer [delete]
export const deletebServer = (data) => {
  return service({
      url: "/cmdb/server/deleteServer",
      method: 'delete',
      data
  })
}

// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "获取server的id"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/findServers [post]
export const getServerById = (data) => {
  return service({
    url: "/cmdb/server/getServerById",
    method: 'post',
    data
  })
}

// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "修改server的id"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router //cmdb/server/updateServer [put]
 export const updateServer = (data) => {
    return service({
      url: "/cmdb/server/updateServer",
      method: 'put',
      data
    })
  }

// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "修改server的id"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router //cmdb/server/updateServerByIdAndStatus [put]
export const updateServerByIdAndStatus = (data) => {
    return service({
        url: "/cmdb/server/updateServerByIdAndStatus",
        method: 'put',
        data
    })
}


// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "修改server的id"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/exportExcel [get]
export const exportExcel = (params) => {
  return service({
    url: "/cmdb/server/exportExcel",
    method: 'get',
    params,
    responseType: 'blob'   // blob文件对象
  }).then(res => {
    handleFileError(res, params.fileName)
  })
}

// @Tags server
// @Summary
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "获取server的id"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/getServersByProjectsList [post]
export const getServersByProjectsList = (data) => {
    return service({
        url: "/cmdb/server/getServersByProjectsList",
        method: 'post',
        data
    })
}
