import service from '@/utils/request'
// @Summary 创建host
// @Produce  application/json
// @Param data body {username:"string",password:"string"}
// @Router /base/resige [post]
export const createHost = (data) => {
        return service({
            url: "/ansible/createansible",
            method: 'post',
            data: data
        })
    }
