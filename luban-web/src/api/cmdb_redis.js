import service from '@/utils/request'
import { handleFileError } from '@/utils/download'

// @Tags CMDBRedis
// @Summary 新增redis
// @Router /cmdb/redis/createCluste[post]
export const createCluster = (data) => {
  return service({
    url: "/cmdb/redis/createCluster",
    method: 'post',
    data
  })
}

// 删除redis集群
export const deleteCluster = (data) => {
  return service({
    url: "/cmdb/redis/deleteCluster",
    method: 'delete',
    data: data
  })
}


// 更新redis集群信息
export const updateCluster = (data) => {
  return service({
    url: "/cmdb/redis/updateCluster",
    method: 'put',
    data: data
  })
}


// 获取列表
export const getClusterList = (data) => {
  return service({
    url: "/cmdb/redis/getClusterList",
    method: 'post',
    data
  })
}

// 通过ID获取redis集群
export const getClusterById = (data) => {
  return service({
    url: "/cmdb/redis/getClusterById",
    method: 'post',
    data
  })
}

// @Tags CMDBRedis
// @Summary Export
// @Router /cmdb/redisAdmin/exportExcel[get]
export const exportExcel = (params) => {
  return service({
    url: "/cmdb/redis/exportExcel",
    method: 'get',
    params,
    responseType: 'blob'   // blob文件对象
  }).then(res => {
    handleFileError(res, params.fileName)
  })
}


/*
redisAdmin 库号 接口
 */

// @Tags CMDBRedisDatabase
// @Summary 新增redis Database记录
// @Router /cmdb/redisAdmin/createRedisDatabase[post]
export const createRedisDatabase = (data) => {
  return service({
    url: "/cmdb/redis/createRedisDatabase",
    method: 'post',
    data
  })
}

// @Tags CMDBRedisDatabase
// @Summary 删除redis Database记录
// @Router /cmdb/redisAdmin/deleteRedisDatabase [delete]
export const deleteRedisDatabase = (data) => {
  return service({
    url: "/cmdb/redis/deleteRedisDatabase",
    method: 'delete',
    data: data
  })
}



// @Tags CMDBRedisDatabase
// @Summary 更新redisDatabase
// @Router /cmdb/redisAdmin/updateRedisDatabase[put]
export const updateRedisDatabase = (data) => {
  return service({
    url: "/cmdb/redis/updateRedisDatabase",
    method: 'put',
    data: data
  })
}

// @Tags CMDBRedisDatabase
// @Summary 获取redis Database列表
// @Router /cmdb/redisAdmin/getRedisDatabaseList[get]
export const getRedisDatabaseList = (data) => {
  return service({
    url: "/cmdb/redis/getRedisDatabaseList",
    method: 'post',
    data
  })
}




// 发送命令
export const sendCommand = (data) => {
  return service({
    url: "/cmdb/redis/command/sendCommand",
    method: 'post',
    data
  })
}

// 获取redis中的db列表
export const getDBList = (query) => {
  return service({
    url: "/cmdb/redis/data/getDBList/" + query,
    method: 'get',
  })
}



// scan key
export const keyScan = (data) => {
  return service({
    url: "/cmdb/redis/key/scan",
    method: 'post',
    data: data
  })
}


// 查询key
export const keyQuery = (data) => {
  return service({
    url: "/cmdb/redis/key/query",
    method: 'post',
    data: data
  })
}


// 根据时间范围查找
// {
//   "cluster_id": 1,
//   "start_time": "1618552743",
//   "end_time": "1618552863",
//   "node_list": ["10.108.26.60:6391","10.108.26.60:6392"]
//
// }
export const getInfoItemMonitorData = (data) => {
  return service({
    url: "/cmdb/redis/monitor/getInfoItemMonitorData",
    method: 'post',
    data
  })
}

