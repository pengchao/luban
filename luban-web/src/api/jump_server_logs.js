import service from '@/utils/request'
// @Tags api
// @Summary 分页获取堡垒机审计日志列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerSshLogs.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/logs/getJumpServerSshLogsList [Get]
// {
//  page     int
//	pageSize int
// }
export const getJumpServerSshLogsList = (params) => {
    return service({
        url: "/jump/logs/getJumpServerSshLogsList",
        method: 'get',
        params
     })
}


// @Tags Api
// @Summary 更新堡垒机审计日志
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpdateJumpServerSshLogsParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /jump/logs/updateDeployApply [post]
export const updateJumpServerSshLogs = (data) => {
    return service({
        url: "/jump/logs/updateJumpServerSshLogs",
        method: 'put',
        data
    })
}



// @Tags Api
// @Summary 删除堡垒机审计日志
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteJumpServerSshLogsParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/logs/deleteJumpServerSshLogs [delete]
export const deleteJumpServerSshLogs = (data) => {
    return service({
        url: "/jump/logs/deleteJumpServerSshLogs",
        method: 'delete',
        data
    })
}

// @Tags Api
// @Summary 批量删除堡垒机审计日志
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteJumpServerSshLogsParams true "删除"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/user/deleteJumpServerSshLogsByIds [delete]
export const deleteJumpServerSshLogsByIds = (data) => {
    return service({
        url: "/jump/logs/deleteJumpServerSshLogsByIds",
        method: 'delete',
        data
    })
}
