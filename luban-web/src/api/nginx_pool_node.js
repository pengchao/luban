import service from '@/utils/request'

// @Tags PoolNode
// @Summary 创建PoolNode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.PoolNode true "创建PoolNode"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/PNode/createNginxPNode [post]
export const createNginxPNode = (data) => {
  return service({
    url: "/nginx/PNode/createNginxPNode",
    method: 'post',
    data
  })
}


// @Tags PoolNode
// @Summary 删除PoolNode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.PoolNode true "删除PoolNode"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /PNode/deletePoolNode [delete]
export const deleteNginxPNode = (data) => {
  return service({
    url: "/nginx/PNode/deleteNginxPNode",
    method: 'delete',
    data
  })
}



// @Tags PoolNode
// @Summary 更新PoolNode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.PoolNode true "更新PoolNode"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /nginx/PNode/deleteNginxPNode [put]
export const updateNginxPNode = (data) => {
  return service({
    url: "/nginx/PNode/updateNginxPNode",
    method: 'put',
    data
  })
}


// @Tags PoolNode
// @Summary 用id查询PoolNode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.PoolNode true "用id查询PoolNode"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /nginx/PNode/getNginxPNodeById [post]
export const getNginxPNodeById = (data) => {
  return service({
    url: "/nginx/PNode/getNginxPNodeById",
    method: 'post',
    data
  })
}


// @Tags PoolNode
// @Summary 分页获取PoolNode列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "分页获取PoolNode列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/PNode/getNginxPNodeList [post]
export const getNginxPNodeList = (data) => {
  return service({
    url: "/nginx/PNode/getNginxPNodeList",
    method: 'post',
    data
  })
}