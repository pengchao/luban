import service from '@/utils/request'

// @Tags Api
// @Summary 获取文件目录
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.GetJumpServerSftpLs true
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /jump/sftp/getJumpServerSftpLs [post]

export const getJumpServerSftpLs = (data) => {
    return service({
        url: "/jump/sftp/getJumpServerSftpLs",
        method: 'post',
        data
    })
}



// @Tags Api
// @Summary 删除文件
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.DeleteJumpServerSftpFile true
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/sftp/deleteJumpServerSftpFile [post]
export const deleteJumpServerSftpFile = (data) => {
    return service({
        url: "/jump/sftp/deleteJumpServerSftpFile",
        method: 'post',
        data
    })
}


// @Tags Api
// @Summary 上传文件
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpLoadJumServerSftpFile true
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/user/upLoadJumServerSftpFile [post]
export const upLoadJumServerSftpFile = (data) => {
    return service({
        url: "/jump/sftp/upLoadJumServerSftpFile",
        method: 'post',
        data
    })
}


// @Tags Api
// @Summary 下载文件
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.DownLoadJumServerSftpFile true
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/sftp/downLoadJumServerSftpFile [get]
export const downLoadJumServerSftpFile = (params) => {
    return service({
        url: "/jump/sftp/downLoadJumServerSftpFile",
        method: 'get',
        params
    })
}
