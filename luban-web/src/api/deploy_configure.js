import service from '@/utils/request'
// @Tags api
// @Summary 分页获取项目应用jenkins job配置列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.ApplyConfigure.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/dispose/getAppConfigureList[Get]
// {
//  page     int
//	pageSize int
// }
export const getAppConfigureList = (params) => {
    return service({
        url: "/deploy/dispose/getAppConfigureList",
        method: 'get',
        params
    })
}


// @Tags Api
// @Summary 更新项目应用配置信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.updateAppConfigureParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /deploy/dispose/updateAppConfigure [post]
export const updateAppConfigure = (data) => {
    return service({
        url: "/deploy/dispose/updateAppConfigure",
        method: 'put',
        data
    })
}


// @Tags Api
// @Summary 新增项目应用配置信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.createAppConfigureParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /deploy/dispose/createAppConfigure [post]

export const createAppConfigure = (data) => {
    return service({
        url: "/deploy/dispose/createAppConfigure",
        method: 'post',
        data
    })
}



// @Tags Api
// @Summary 删除项目应用信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteAppConfigureParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /deploy/dispose/deleteAppConfigure [delete]
export const deleteAppConfigure = (data) => {
    return service({
        url: "/deploy/dispose/deleteAppConfigure",
        method: 'delete',
        data
    })
}

// @Tags Api
// @Summary 拷贝项目应用配置信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.DeployApplyCopyParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /deploy/dispose/copyAppConfigure [post]
export const copyAppConfigure = (data) => {
    return service({
        url: "/deploy/dispose/copyAppConfigure",
        method: 'post',
        data
    })
}
