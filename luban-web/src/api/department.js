import service from '@/utils/request'
// @Tags api
// @Summary 分页获部门色列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页获取部门列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /department/getDepartmentList [post]
// {
//  page     int
//	pageSize int
// }
export const getDepartmentList = (data) => {
  return service({
    url: "/department/getDepartmentList",
    method: 'post',
    data
  })
}
// @Tags Api
// @Summary 新增部门
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.createDepartmentParams true "新增部门"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /department/createDepartment[post]

export const createDepartment = (data) => {
  return service({
    url: "/department/createDepartment",
    method: 'post',
    data
  })
}

// @Tags SysUser
// @Summary 删除部门
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SetUserAuth true "删除部门"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"修改成功"}"
// @Router /department/deleteDepartment [delete]
export const deleteDepartment = (data) => {
  return service({
    url: "/department/deleteDepartment",
    method: 'delete',
    data: data
  })
}

// @Tags Api
// @Summary 根据Id查询公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.getDepartmentById  true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /department/getDepartmentById[get]

export const getDepartmentById = (data) => {
  return service({
    url: "/department/getDepartmentById",
    method: 'post',
    data: data
  })
}

// @Tags Api
// @Summary 更新部门信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.updateDepartment  true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /department/updateDepartment[put]

export const updateDepartment = (data) => {
  return service({
    url: "/department/updateDepartment",
    method: 'put',
    data: data
  })
}


// @Tags Api
// @Summary 查询部门信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.getDepartment  true "查询部门信息"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /department/getDepartment[get]

export const getDepartmentByCode = (params) => {
  return service({
    url: "/department/getDepartmentByCode",
    method: 'get',
    params
  })
}

