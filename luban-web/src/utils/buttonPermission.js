
// 是否有发布生产的权限-进行过滤
export const  hasDeployProdPermission = (userInfo, appInfo) => {
  if (userInfo.deploy_prod == false) {
    appInfo = appInfo.filter(x => x.deploy_env != "prod")
  }
  return appInfo
}