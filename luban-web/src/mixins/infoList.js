import { getDict } from "@/utils/dictionary";
export default {
    data() {
        return {
            page: 1,
            total: 10,
            pageSize: 10,
            tableData: [],
            searchInfo: {}
        }
    },
    methods: {
        filterDict(value, type) {
            const rowLabel = this[type + "Options"] && this[type + "Options"].filter(item => item.value == value)
            return rowLabel && rowLabel[0] && rowLabel[0].label
        },
        async getDict(type) {
            const dicts = await getDict(type)
            this[type + "Options"] = dicts
            return dicts
        },
        handleSizeChange(val) {
            this.pageSize = val
            this.getTableData()
        },
        handleCurrentChange(val) {
            this.page = val
            this.getTableData()
        },
        async getTableData(page = this.page, pageSize = this.pageSize) {
            // 删除空的字符串
            for ( let key in this.searchInfo ){
                if ( this.searchInfo[key] === '' ){
                    delete this.searchInfo[key]
                }
            }
            const table = await this.listApi({ page, pageSize, ...this.searchInfo })
            if (table.code == 0) {
                this.tableData = table.data.list
                this.total = table.data.total
                this.page = table.data.page
                this.pageSize = table.data.pageSize
            }
        },
        // 获取字典的字典项 列表
        getDicItemList(dict_code) {
            const dictionaryList = JSON.parse(localStorage.getItem('dictionaryList'));
            for(let i=0,len=dictionaryList.length;i<len;i++){
                if (dictionaryList[i].dict_code == dict_code) {
                    return dictionaryList[i].sysDictionaryDetails
                }
            }
        },
        //获取项目权限
        getProjectAuthList(){
            const projectAuthList = JSON.parse(localStorage.getItem('projectAuth'));
            return projectAuthList
        }
    }
}
